package com.techuniversity.facs.productos;

import com.techuniversity.facs.facturas.FacturaModel;
import org.springframework.data.repository.CrudRepository;

public interface ProductoRepository extends CrudRepository<ProductoModel, Integer> {

}
